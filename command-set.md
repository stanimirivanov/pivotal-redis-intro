### Preparation

Make sure that your Redis server is running.  Navigate to the directory of your Redis command-line interface executable, and examine the 
list of options available for the command-line interface by entering __redis-cli --help__. 

Connect to your Redis server.  Note that you can connect to a specified server by providing the host name or IP address and port as optional 
parameters using the -h and -p options.

#### Working With Keys

Set the key "simple_key" to the value "a value", then check to see that the value has been set correctly.

```
127.0.0.1:6379> set simple_key 'a value'
OK
127.0.0.1:6379> get simple_key
"a value"
```

Create the following users with keys formatted as "user:id":  id=1 Bill, id=2 John, id=3 Sandeep.

```
127.0.0.1:6379> set user:1 Bill
OK
127.0.0.1:6379> get user:1
"Bill"
127.0.0.1:6379> set user:2 John
OK
127.0.0.1:6379> set user:3 Sandeep
OK
```

Fetch a list of all the keys that you've created.

```
127.0.0.1:6379> keys *
1) "user:1"
2) "simple_key"
3) "user:2"
4) "user:3"
```

Fetch a list of just the user keys.

```
127.0.0.1:6379> keys user:*
1) "user:1"
2) "user:2"
3) "user:3"
```

Create 2 more users:  id=11 Sam, id=21 Lee

```
127.0.0.1:6379> set user:11 Sam
OK
127.0.0.1:6379> set user:21 Lee
OK
```

Fetch a list of all the user keys for id's ending in 1.

```
127.0.0.1:6379> keys user:?1
1) "user:21"
2) "user:11"
```

Create user id=31 Dan

```
127.0.0.1:6379> set user:31 Dan
OK
```

Fetch a list of user keys with id's ending in 1 between 20 and 39.

```
127.0.0.1:6379> keys user:[23]1
1) "user:21"
2) "user:31"
```

Change the user:1 key to user:01.

```
127.0.0.1:6379> rename user:1 user:01
OK
```

Fetch a list of all the keys ending in 1.

```
127.0.0.1:6379> keys user:?1
1) "user:21"
2) "user:01"
3) "user:31"
4) "user:11"
```

Delete user with id=3.

```
127.0.0.1:6379> del user:3
(integer) 1

127.0.0.1:6379> keys user:*
1) "user:21"
2) "user:01"
3) "user:31"
4) "user:11"
5) "user:2"
```

Set user id=2 to expire in 10 seconds.

```
127.0.0.1:6379>pexpire user:2 10000
(integer) 1
```

#### Working With Strings

Set the key "animal" to the value "dog".

```
127.0.0.1:6379> set animal dog
OK
```

Change the value of the animal key from "dog" to "sea", returning the original value.

```
127.0.0.1:6379> getset animal sea
"dog"
```

Append "horse" to the value of animal.

```
127.0.0.1:6379> append animal horse
(integer) 8
```

Fetch the substring "horse".

```
127.0.0.1:6379> getrange animal 3 -1
"horse"
```

Fetch the substring "sea".

```
127.0.0.1:6379> getrange animal 0 2
"sea"
```

Define a new string "count" to the value of 0.

```
127.0.0.1:6379> set count 0
OK
```

Increment count by 1.

```
127.0.0.1:6379> incr count
(integer) 1

127.0.0.1:6379> get count
"1"
```

Increment count by 10.

```
127.0.0.1:6379> incrby count 10
(integer) 11

127.0.0.1:6379> get count
"11"
```

Get the length of the count string.

```
127.0.0.1:6379> strlen count
(integer) 2
```

#### Working With Hashes

First, check to see if the key "dogs" exists in the "animals" hash.

```
127.0.0.1:6379> hexists animals dog
(integer) 0
```

Create a hash "animals", and add "dogs" with a value of 25 to it.

```
127.0.0.1:6379> hset animals dogs 25
(integer) 1
```

Add "cats" with a value of 37 to the hash.

```
127.0.0.1:6379> hset animals cats 37
(integer) 1
```

Add "fish" with a value of 28 to the hash.

```
127.0.0.1:6379> hset animals fish 28
(integer) 1
```

Return the entire contents (keys and values) of the "animals" hash.

```
127.0.0.1:6379> hgetall animals
1) "dogs"
2) "25"
3) "cats"
4) "37"
5) "fish"
6) "28"
```

Return just the values of the dogs and cats entries.

```
127.0.0.1:6379> hmget animals dogs cats
1) "25"
2) "37"
```

In a single command, create a new hash "trees" and add "maple" with a value of 83, and "oak" with a value of 79 to the hash.

```
127.0.0.1:6379> hmset trees maple 83 oak 72
OK
127.0.0.1:6379> hvals trees
1) "83"
2) "79"
127.0.0.1:6379> hkeys trees
1) "maple"
2) "oak"
```

Display just the values of the trees hash.

```
127.0.0.1:6379> hgetall trees
1) "maple"
2) "83"
3) "oak"
4) "79"
```

Display just the keys in the trees hash.

```
127.0.0.1:6379> hset trees oak 72
(integer) 0
```

Correct the value of the "oak" key to 72, and verify.

```
127.0.0.1:6379> hget trees oak
"72"
```

#### Working With Lists

Add the value "Check mail" to end of the "tasks" list, but using the command that adds it only if the list already exists.  
(Since it shouldn't exist at this point, note the return value of the command.)

```
127.0.0.1:6379> rpushx tasks "Check mail"
(integer) 0
```

Repeat the previous step, but use the version of the command that doesn't check for existence.

```
127.0.0.1:6379> rpush tasks "Check mail"
(integer) 1
```

Append the value "Open mail" to the end of the list.

```
127.0.0.1:6379> rpush tasks "Open mail"
(integer) 2
```

Prepend "Start system" to the list.

```
127.0.0.1:6379> lpush tasks "Start system"
(integer) 3
```

Get the length of the list.

```
127.0.0.1:6379> llen tasks
(integer) 3
```

Return the contents of the entire list.

```
127.0.0.1:6379> lrange tasks 0 -1
1) "Start system"
2) "Check mail"
3) "Open mail"
```

Set the value of the first entry in the list to "Open mail".

```
127.0.0.1:6379> lset tasks 0 "Open mail"
OK
```

Using a single command, examine the first 2 entries in the list.

```
127.0.0.1:6379> lrange tasks 0 1
1) "Open mail"
2) "Check mail"
```

Remove the first entry in the list.

```
127.0.0.1:6379> lpop tasks
"Open mail"
```

Examine the value of the second entry in the list.

```
127.0.0.1:6379> lindex tasks 1
"Open mail"
```

Return the entire list.

```
127.0.0.1:6379> lrange tasks 0 -1
1) "Check mail"
2) "Open mail"
```

#### Working With Sets

First, add "apples", "oranges", and "bananas" to a set called "basket:1".

```
127.0.0.1:6379> sadd basket:1 apples
(integer) 1
127.0.0.1:6379> sadd basket:1 oranges
(integer) 1
127.0.0.1:6379> sadd basket:1 bananas
(integer) 1
```

List the members of "basket:1".

```
127.0.0.1:6379> smembers basket:1
1) "bananas"
2) "apples"
3) "oranges"
```

Add "pineapples", "bananas", and "oranges" to a "basket:2" set.

```
127.0.0.1:6379> sadd basket:2 pineapples
(integer) 1
127.0.0.1:6379> sadd basket:2 bananas
(integer) 1
127.0.0.1:6379> sadd basket:2 oranges
(integer) 1
```

Verify the members of "basket:2".

```
127.0.0.1:6379> smembers basket:2
1) "bananas"
2) "pineapples"
3) "oranges"
```

Get the intersection of the 2 sets.

```
127.0.0.1:6379> sinter basket:1 basket:2
1) "bananas"
2) "oranges"
```

Move "pineapples" from "basket:2" to "basket:1", and verify by listing the members.

```
127.0.0.1:6379> smove basket:2 basket:1 pineapples
(integer) 1

127.0.0.1:6379> smembers basket:1
1) "pineapples"
2) "bananas"
3) "apples"
4) "oranges"
```

Store the union of the 2 sets into a new set called "allbaskets".

```
127.0.0.1:6379> sunionstore allbaskets basket:1 basket:2
(integer) 4

127.0.0.1:6379> smembers allbaskets
1) "apples"
2) "oranges"
3) "pineapples"
4) "bananas"
```

Remove "oranges" from "basket:1".

```
127.0.0.1:6379> srem basket:1 oranges
(integer) 1

127.0.0.1:6379> smembers basket:1
1) "pineapples"
2) "bananas"
3) "apples"
```

Working With Sorted Sets

Add the following members and scores to the "team:1" sorted set:  John 69, Lee 67, Sandeep 70, Sandra 68, Ann 70, and Mary 73.

```
zadd team:1 71 John 67 Lee 70 Sandeep 68 Sandra 70 Anne 73 Mary
(integer) 6
```

Fetch the number of members in the set.

```
127.0.0.1:6379> zcard team:1
(integer) 6
```

Get a count of the number of members with scores between 70 and 75.

```
127.0.0.1:6379> zcount team:1 70 75
(integer) 4
```

Get the members with scores between 65 and 70.

```
127.0.0.1:6379> zrangebyscore team:1 65 70
1) "Lee"
2) "Sandra"
3) "Anne"
4) "Sandeep"

127.0.0.1:6379> zrank team:1 Lee
(integer) 0
```

Return the rank of Sandra on the team.

```
127.0.0.1:6379> zrank team:1 Sandra
(integer) 1
```

Return, in descending order, the members between 65 and 69.

```
127.0.0.1:6379> zrevrangebyscore team:1 69 65
1) "Sandra"
2) "Lee"
```

#### Using Publish-Subscribe (Pub-Sub) in Redis

In your Redis CLI, subscribe to the "notifications" channel.

```
CLI #1
$ ./redis-cli
127.0.0.1:6379> subscribe notifications
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "notifications"
3) (integer) 1

```

Open another window, start a second Redis CLI session, and connect to your Redis server.

```
CLI #2
$ ./redis-cli
127.0.0.1:6379>
```

In the second window, publish some messages to the notifications channel. Observe the subscription window, you should see the meessages received there.

```
CLI #2
127.0.0.1:6379> publish notifications hello
(integer) 1

127.0.0.1:6379> publish notifications redis
(integer) 1
```

Open a third window and subscribe to the notifications channel.

```
CLI #3
$ ./redis-cli
127.0.0.1:6379> subscribe notifications
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "notifications"
3) (integer) 1
```

Go back to your second window, and examine the number of subscribers on the notifications channel.

```
CLI #2
127.0.0.1:6379> pubsub numsub notifications
1) "notifications"
2) "2"
```

In the second window and publish more messages.  Observe the other subscriber windows, as well as the responses from the publish command.

```
CLI #2
127.0.0.1:6379> publish notifications "hello redis"
(integer) 2

CLI #1
127.0.0.1:6379> subscribe notifications
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "notifications"
3) (integer) 1
1) "message"
2) "notifications"
3) "hello"
1) "message"
2) "notifications"
3) "redis"
1) "message"
2) "notifications"
3) "hello redis"

CLI #3
127.0.0.1:6379> subscribe notifications
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "notifications"
3) (integer) 1
1) "message"
2) "notifications"
3) "hello redis"
```

Remove all subscriptions from the channel (you'll have to issue a ctrl-c in the CLI).

```
CLI #1
^C
$

CLI #3
^C
$
```

Publish at least one more message to the notifications channel.  Note the return value of the publish now, as compared to earlier.

```
127.0.0.1:6379> publish notifications "final hello"
(integer) 0
```

Restart one of the subscriptions, and note that messages published while the subscriber was down have been lost.

```
CLI #1
$ ./redis-cli
127.0.0.1:6379> subscribe notifications
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "notifications"
3) (integer) 1

```

Remove the subscription from the channel.

```
CLI #1
^C
$
```

#### Working With Transactions

Start a transaction.

```
CLI #1
127.0.0.1:6379> multi
OK
```

Insert some strings into Redis.

```
CLI #1
127.0.0.1:6379> set house bungalow
QUEUED

127.0.0.1:6379> set car sedan
QUEUED
```

Abort the transaction.

```
CLI #1
127.0.0.1:6379> discard
OK
```

Start a new transaction.

```
CLI #1
127.0.0.1:6379> multi
OK
```

Insert some strings into Redis.

```
CLI #1
127.0.0.1:6379> set house bungalow
QUEUED

127.0.0.1:6379> set car sedan
QUEUED
```

In another Redis CLI window, attempt to fetch the values of the strings you just inserted. 
Note that no results are returned, because the transaction has not yet been committed.

```
CLI #2
127.0.0.1:6379> get house
(nil)

127.0.0.1:6379> get car
(nil)
```

Commit the transaction.

```
CLI #1
127.0.0.1:6379> exec
1) OK
2) OK
```

In the other Redis CLI window, again fetch the values of the strings you just inserted. 
Now the values should be returned since the transaction was committed.

```
CLI #2
127.0.0.1:6379> get house
"bungalow"
127.0.0.1:6379> get car
"sedan"
```

#### Connection and Server Management Commands

Ping the server.

```
127.0.0.1:6379> ping
PONG
```

Open a second CLI window and connect to the server.

```
$ ./redis-cli
127.0.0.1:6379>
```

From the first window, list the client connections.

```
127.0.0.1:6379> client list
addr=127.0.0.1:34429 fd=7 name= age=3387 idle=682 flags=N db=0 sub=0 psub=0 multi=-1 qbuf=0 qbuf-free=0 obl=0 oll=0 omem=0 events=r cmd=get
addr=127.0.0.1:34432 fd=6 name= age=1037 idle=0 flags=N db=0 sub=0 psub=0 multi=-1 qbuf=0 qbuf-free=32768 obl=0 oll=0 omem=0 events=r cmd=client

127.0.0.1:6379> client getname
(nil)
```

Set the configuration parameter "masterauth" to "secretpassword".

```
127.0.0.1:6379> config set masterauth secretpassword
OK
```

Get the "masterauth" configuration parameter to verify it was set correctly.

```
127.0.0.1:6379> config get masterauth
1) "masterauth"
2) "secretpassword"
```

Run the "info" command to display current statistics about the server.

```
127.0.0.1:6379> info
# Server
redis_version:2.8.8
redis_git_sha1:00000000
redis_git_dirty:0
redis_build_id:5a7937136cbe1a52
redis_mode:standalone
os:Linux 2.6.32-220.el6.x86_64 x86_64
arch_bits:64
multiplexing_api:epoll
gcc_version:4.4.6
process_id:2253
run_id:e6f97898d5d2ffc2b8cf6b015262e78fbcab2508
tcp_port:6379
uptime_in_seconds:23919
uptime_in_days:0
hz:10
lru_clock:8721188
config_file:

# Clients
connected_clients:2
client_longest_output_list:0
client_biggest_input_buf:0
blocked_clients:0

# Memory
used_memory:832968
used_memory_human:813.45K
used_memory_rss:7979008
used_memory_peak:852992
used_memory_peak_human:833.00K
used_memory_lua:33792
mem_fragmentation_ratio:9.58
mem_allocator:jemalloc-3.2.0

# Persistence
loading:0
rdb_changes_since_last_save:0
rdb_bgsave_in_progress:0
rdb_last_save_time:1401228882
rdb_last_bgsave_status:ok
rdb_last_bgsave_time_sec:0
rdb_current_bgsave_time_sec:-1
aof_enabled:0
aof_rewrite_in_progress:0
aof_rewrite_scheduled:0
aof_last_rewrite_time_sec:-1
aof_current_rewrite_time_sec:-1
aof_last_bgrewrite_status:ok
aof_last_write_status:ok

# Stats
total_connections_received:5
total_commands_processed:36
instantaneous_ops_per_sec:0
rejected_connections:0
sync_full:0
sync_partial_ok:0
sync_partial_err:0
expired_keys:0
evicted_keys:0
keyspace_hits:8
keyspace_misses:2
pubsub_channels:0
pubsub_patterns:0
latest_fork_usec:20111

# Replication
role:master
connected_slaves:0
master_repl_offset:0
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0

# CPU
used_cpu_sys:2.19
used_cpu_user:0.81
used_cpu_sys_children:0.08
used_cpu_user_children:0.00

# Keyspace
db0:keys=17,expires=0,avg_ttl=0
```

Shutdown the server gracefully.

```
127.0.0.1:6379> shutdown
[2253] 27 May 18:39:05.487 # User requested shutdown...
[2253] 27 May 18:39:05.487 * Saving the final RDB snapshot before exiting.
[2253] 27 May 18:39:05.490 * DB saved on disk
[2253] 27 May 18:39:05.490 # Redis is now ready to exit, bye bye...
```
