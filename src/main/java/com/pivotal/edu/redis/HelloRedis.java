package com.pivotal.edu.redis;

import redis.clients.jedis.Jedis;

/**
 * Hello Redis!
 *
 */
public class HelloRedis {
	
	public static void main(String[] args) {
		// TODO-01: Open a connection to Redis
		Jedis jedis = new Jedis("127.0.0.1");

		try {
			// Define the key
			String key = "Hello";

			// TODO-02: insert the key-value pair into Redis
			jedis.set(key, "Redis!");

			// TODO-03: Fetch the value, length, and first three characters from the
			// server
			String value = jedis.get(key);
			long valLen = jedis.strlen(key);
			String subString = jedis.getrange(key, 0, 2);

			// Print the values to the console
			System.out.println(key + " " + value + ", length=" + valLen
					+ ", substring=" + subString);

			// TODO-04: Delete the key from the server
			jedis.del(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// TODO-05: Close the connection
			jedis.close();
		}
		
	}
}
