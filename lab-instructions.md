### Hello Redis

To begin, let's write code to establish a connection to the Redis server, and perform a few simple operations to demonstrate the use of strings.

#### INSTRUCTIONS

1. Write the code to insert the key "Hello" and value "Redis" into the server. 
Fetch the value itself back from the server, it's length, and the first 3 characters and print them to the console. 
Then delete the key and close the connection.

2. Run and test your code.

### Working with Other Redis Datastructures

Let's see how some of the other Redis commands for working with hashes, lists, and sets translate to programmatic method calls.

#### INSTRUCTIONS

1. Open a connection to your Redis server.

2. Create an `updateOrderStatus` method that takes an order ID string and a status string, and stores it into a Redis hash with a key of the 
format "order:<orderId>".

3. Create a `storeOrderItems` method that takes an order ID string and a string array of order item ID's, and stores them into a set in Redis.

4. Create an `addOrderToProcessList` method that takes an order ID string and appends it to a Redis list with the key "OrdersToProcess".

5. In the main method of your program, create an `orderID` and an `orderItemIds` string array, and call the 3 methods passing in the appropriate 
parameters.

6. Run and test your code.  Use the Redis CLI to check the values were inserted correctly. Once you are satisfied the code is working properly, 
clean out the data by issuing the `flushdb` command from the Redis CLI.


__Redis CLI Output:__
```
127.0.0.1:6379> hgetall order:6884
1) "status"
2) "received"
3) "id"
4) "6884"
127.0.0.1:6379> smembers orderItems:6884
1) "20003"
2) "73332"
3) "855749"
127.0.0.1:6379> lrange OrdersToProcess 0 -1
1) "6884"
127.0.0.1:6379>
```

### Using Transactions

Let's take a look at a simple example using Redis transactions to make our operations atomic.  We will modify the previous exercise to execute all the operations in a transaction.

#### INSTRUCTIONS

1. The return values for each of the data insertion methods has been modified to return the corresponding transaction response object. 
In addition, modify each method to accept a Transaction input parameter.

2. Change the Jedis method calls that insert the data to use the transaction parameter passed in, and return the results of the call.

3. Modify the main method to start, execute, and print out the returned transaction results of each method call. 
Note that you cannot print out the responses until after the transaction has been executed.

4. Run and test your code.  Flush the server when you are done.

__Output:__
```
rsp1=OK
rsp2=3
rsp3=1
```